import random

class Carte:
    def __init__(self, rang, couleur):
        self.rang = rang
        self.couleur = couleur

    def nom_carte(self):
        if self.rang <= 10:
            rang = str(self.rang)
        elif self.rang == 11:
            rang = "Valet"
        elif self.rang == 12:
            rang = "Reine"
        elif self.rang == 13:
            rang = "Roi"
        elif self.rang == 14:
            rang = "As"
        return rang + " de " + self.couleur

class JeuDeCartes:
    def __init__(self):
        self.deck = []
        self.creer_deck()
        random.shuffle(self.deck)

    def creer_deck(self):
        for couleur in ("Coeur", "Trèfle", "Carreau", "Pique"):
            for rang in range(2, 15):
                self.deck.append(Carte(rang, couleur))

    def prendre_carte(self, joueur):
        carte = self.deck[0]
        self.deck.remove(self.deck[0])
        print(joueur + " a pris " + carte.nom_carte())
        return carte

class PartieDeBataille:
    def __init__(self):
        self.joueur_un_score = 0
        self.joueur_deux_score = 0
        self.manche = 0
        self.jeu_de_cartes = JeuDeCartes()

    def jouer(self):
        while True:
            self.manche += 1
            print("##############" + str(self.manche) + "#############")
            joueur_un_carte = self.jeu_de_cartes.prendre_carte("joueur 1")
            joueur_deux_carte = self.jeu_de_cartes.prendre_carte("joueur 2")

            if joueur_un_carte.rang > joueur_deux_carte.rang:
                gagnant = "joueur 1"
                self.joueur_un_score += 2
            elif joueur_deux_carte.rang > joueur_un_carte.rang:
                gagnant = "joueur 2"
                self.joueur_deux_score += 2
            else:
                gagnant = "egalité!!!  Personne"

            print(gagnant + " a gagné")

            if len(self.jeu_de_cartes.deck) == 0:
                print("jeu fini le deck est vide")
                print("Joueur 1 a " + str(self.joueur_un_score) + " points")
                print("Joueur 2 a " + str(self.joueur_deux_score) + " points")
                break

        if self.joueur_un_score > self.joueur_deux_score:
            print("Joueur 1 a gagné le match avec " + str(self.joueur_un_score) + " points. Félicitations !!!")
        elif self.joueur_deux_score > self.joueur_un_score:
            print("Joueur 2 a gagné le match avec " + str(self.joueur_deux_score) + " points. Félicitations !!!")
        else:
            print("Match nul, personne n'a gagné le match")


partie = PartieDeBataille()
partie.jouer()
